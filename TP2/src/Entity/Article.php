<?php

namespace App\Entity;

use App\Repository\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ArticleRepository::class)]
class Article
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $titre = null;

    #[ORM\ManyToMany(targetEntity: Tag::class, inversedBy: 'articles')]
    private Collection $ref_tag;

    #[ORM\ManyToOne(inversedBy: 'articles')]
    private ?Auteur $ref_auteur = null;

    public function __construct()
    {
        $this->ref_tag = new ArrayCollection();
    }

    public function __toString(): string
    {
        // TODO: Implement __toString() method.
        return $this->titre;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): static
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * @return Collection<int, Tag>
     */
    public function getRefTag(): Collection
    {
        return $this->ref_tag;
    }

    public function addRefTag(Tag $refTag): static
    {
        if (!$this->ref_tag->contains($refTag)) {
            $this->ref_tag->add($refTag);
        }

        return $this;
    }

    public function removeRefTag(Tag $refTag): static
    {
        $this->ref_tag->removeElement($refTag);

        return $this;
    }

    public function getRefAuteur(): ?Auteur
    {
        return $this->ref_auteur;
    }

    public function setRefAuteur(?Auteur $ref_auteur): static
    {
        $this->ref_auteur = $ref_auteur;

        return $this;
    }

}
