# Requirements

### Symfony CLI 
https://symfony.com/download

Run `symfony check:requirements` to check local requirements

### Version
PHP 8.3

Composer (use project composer.phar)

# Install

```
cd app/
php composer.phar install
```

# Run
`symfony server:start`

## Local Container DB
In local you can use docker to handle db
`docker-compose up -d`


# Troubleshooting
## Permission cache, logs
https://symfony.com/doc/current/setup/file_permissions.html