<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use App\Entity\Auteur;
use App\Entity\Tags;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
          ->setTitle('DDB - Gestion des droits - Administration')
          ->renderContentMaximized();
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToCrud('Les Auteurs', 'fa-solid fa-user', Auteur::class);
        yield MenuItem::linkToCrud('Les Articles', 'fa-solid fa-newspaper', Article::class);
        yield MenuItem::linkToCrud('Les Tags', 'fa-solid fa-tag', Tags::class);
    }
}
